# ShortURLy

Very basic URL shortener.

## Usage

1. Go to the application's page(after deploying it).
1. Enter the URL to be shortened in the text field.
1. Click "Generate".
1. Copy and share the generated URL.
1. Enter the generated URL in your browser's address tab and voila!

## Installation/Deployment Steps

This project only provisions for containerized deployment on Kubernetes cluster.
You can try to derive a Docker Compose based deployment from this section if you like.
It will look similar to the `docker-compose.yaml` file in this project, with some differences here and there.

For all objects defined in the files in each subsection, set the `metadata.namespace` to the namespace of your choice. Ignore if deploying to `default` namespace or you have set the preferred namespace as default namespace via Kubeconfig.

### Set up the Secrets

1. Get permission to access the project's container registry via deploy token.
    - Without such access, copy out `docker/Dockerfile`, follow the script in `build_pd` job(replace all the CI refname and tag with the appropriate released version), build the image locally and push to your own registry.
1. Copy out `kubernetes/secrets.yaml`.
    - Set the `.dockerconfigjson` field of `registry-creds` secret accordingly.
    - Replace all `xxx` with Base64-encoded strings of various passwords.
    - Make sure `MYSQL_PASSWORD` and `DATABASE_PASSWORD` have the same value.
    - Once done, create the secrets.
        ```
        kubectl apply -f secrets.yaml
        ```

### Set up the services

1. Copy out `kubernetes/services.yaml`.
    - Read the comments and adjust the fields/values according to your cluster environment.
    - You may or may not have to create your own Ingress depending on whether the service is a `loadBalancer` or `ClusterIP` type.
    - Set up the services.
        ```
        kubectl apply -f services.yaml
        ```

### Set up the application components

1. Copy out `kubernetes/db.yaml`.
    - Read through the comments and adjust the fields/values accordingly.
    - Deploy the database.
        ```
        kubectl apply -f db.yaml
        ```
    - Check that the database pod is in running state before proceeding.
1. Copy out `kubernetes/web.yaml`.
    - Set the `DATABASE_HOST` field accordingly. In particular, take note of the namespace and cluster domain portions of the FQDN(check for exact value for your cluster's platform).
    - Replace `${CI_COMMIT_TAG}` with the desired released version.
    - Deploy the web application.
        ```
        kubectl apply -f web.yaml
        ```

## Confirming access to application

- Check that the application is accessible via an external IP address(ie. either of the following objects have an IP address under the `External-IP` column).
    - If using LoadBalancer type service.
        ```
        kubectl get services
        ```
    - If using Ingress.
        ```
        kubectl get ingresses
        ```
- Fix public access issues.
    - LoadBalancer IP address requires that it is either provisioned automatically by the cloud platform, or you manually obtain it and set it in the service definition itself.  
    For self-deployed clusters, consider load-balancing solutions like MetalLB.
    - Ingress requires an ingress controller installed in the cluster. For public cloud platforms, this is not an issue.  
    For self-deployed clusters, consider Nginx Ingress Controller(set `ingressClassName: nginx` in your Ingress definition). This requires a load balancer installed for the cluster as well, which you can consider MetalLB.
    - Check that the nodes running the pods have access to the Internet. You may need to add/tweak firewall rules and/or NAT(configurable with cloud provider).
    - Anything else is typically specific to the cluster's network environment, which cannot be covered in this documentation.