package main

import (
	"context"
	"database/sql"
	"html/template"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/mysql"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/gorilla/mux"
	"gitlab.com/zixian92/shorturly/api"
	"gitlab.com/zixian92/shorturly/database"
	"gitlab.com/zixian92/shorturly/store"
)

var Version string

func main() {
	logger := log.New(os.Stderr, "", log.LstdFlags)
	logger.Printf("ShortURLy version %s", Version)

	// Set up database connection
	logger.Printf("setting up database connection")
	dbPortStr := os.Getenv("DATABASE_PORT")
	var dbPort uint64
	var err error
	var dbDriver *sql.DB
	if dbPortStr != "" {
		dbPort, err = strconv.ParseUint(dbPortStr, 10, 16)
		if err != nil {
			logger.Fatalf("invalid database port %s", dbPortStr)
		}
	}
	dbDriver, err = database.InitDB(database.Config{
		Type:     database.DatabaseTypeMySQL,
		Host:     os.Getenv("DATABASE_HOST"),
		Port:     uint16(dbPort),
		User:     os.Getenv("DATABASE_USER"),
		Password: os.Getenv("DATABASE_PASSWORD"),
		Database: os.Getenv("DATABASE_NAME"),
	})
	if err != nil {
		logger.Fatalf("failed to set up database connection: %v", err)
	}
	logger.Printf("database connection established")

	// Migrating database schema
	logger.Printf("starting database migration")
	migrationDriver, err := mysql.WithInstance(dbDriver, &mysql.Config{})
	if err != nil {
		database.Shutdown()
		logger.Fatalf("failed to set up driver for database schema migration: %v", err)
	}
	migrator, err := migrate.NewWithDatabaseInstance(
		"file:///var/lib/shorturly/migrations",
		string(database.DatabaseTypeMySQL), migrationDriver)
	if err != nil {
		database.Shutdown()
		logger.Fatalf("failed to set up database migrator: %v", err)
	}
	if err = migrator.Up(); err != nil && err != migrate.ErrNoChange {
		migrator.Close()
		logger.Fatalf("database schema migration failed: %v", err)
	}
	dbVer, _, _ := migrator.Version()
	logger.Printf("database schema version is %d", dbVer)
	logger.Printf("migrated database schema")

	// Set up data store
	store.InitStores(database.GetDB())
	logger.Printf("initialized data stores")

	// Set up endpoints
	mainRouter := mux.NewRouter()

	// Set up API routes
	apiRouter := mainRouter.PathPrefix("/api/").Subrouter()
	api.InitAPIRoutes(apiRouter, logger)

	// Serve static assets
	mainRouter.PathPrefix("/ui/").Handler(http.StripPrefix("/ui/", http.FileServer(http.Dir("/var/lib/shorturly/ui")))).Methods("GET")

	// Handle everything that is not / as short URLs
	mainRouter.HandleFunc("/{id:\\w+}", api.HandleShortURL).Methods("GET")

	// Placeholder, to render HTML page once front-end is up
	mainRouter.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		htmlTemplate, err := template.ParseFiles("/var/lib/shorturly/ui/index.html")
		if err != nil {
			logger.Printf("failed to load HTML file: %v", err)
			http.Error(w, "server error loading HTML file", http.StatusInternalServerError)
			return
		}
		htmlData := struct {
			Title string
		}{
			Title: "ShortURLy",
		}
		if err := htmlTemplate.Execute(w, htmlData); err != nil {
			logger.Printf("failed to set data in HTML file: %v", err)
			http.Error(w, "server error serving HTML file", http.StatusInternalServerError)
		}
	}).Methods("GET")

	// Set up web server
	webServer := &http.Server{
		Handler:  mainRouter,
		ErrorLog: logger,
	}

	// Prepare concurrent components handling
	appContext := context.Background()
	components := sync.WaitGroup{}
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)

	// Run the web server
	logger.Printf("starting web server")
	webServerErrs := make(chan error, 1)
	components.Add(1)
	go func() {
		err := webServer.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			webServerErrs <- err
		}
		components.Done()
	}()

	select {
	case <-sigChan:
		logger.Printf("beginning shutdown")

		// Shut down web server
		webServerShudownContext, cancelWebServerShutdown := context.WithTimeout(appContext, 3*time.Second)
		err := webServer.Shutdown(webServerShudownContext)
		if err != nil && err != http.ErrServerClosed {
			logger.Printf("web server shut down with error: %v", err)
		}
		cancelWebServerShutdown()

		// Close database connection
		if err := database.Shutdown(); err != nil {
			logger.Printf("error closing database connection: %v", err)
		}
	case webServerErr := <-webServerErrs:
		logger.Printf("web server failed: %v", webServerErr)
	}

	logger.Printf("waiting for other components to terminate")
	components.Wait()
	logger.Printf("all components terminated. Bye...")
}
