"use strict"

import { useEffect, useState } from "react"
import { createPortal, render } from "react-dom"
import { createTheme, ThemeProvider } from "@mui/material/styles"

import Button from "@mui/material/Button"
import CloseIcon from "@mui/icons-material/Close"
import CssBaseline from "@mui/material/CssBaseline"
import DeleteIcon from "@mui/icons-material/Delete"
import Dialog from "@mui/material/Dialog"
import DialogActions from "@mui/material/DialogActions"
import DialogContent from "@mui/material/DialogContent"
import DialogContentText from "@mui/material/DialogContentText"
import DialogTitle from "@mui/material/DialogTitle"
import Grid from "@mui/material/Grid"
import IconButton from "@mui/material/IconButton"
import Link from "@mui/material/Link"
import LinkedInIcon from "@mui/icons-material/LinkedIn"
import Paper from "@mui/material/Paper"
import Snackbar from "@mui/material/Snackbar"
import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"
import TextField from "@mui/material/TextField"
import Typography from "@mui/material/Typography"

const appTheme = createTheme({
  palette: {
    mode: "dark",
  },
})

const disclaimers = [
  `This application is just a proof-of-concept, or prototype, developed as a take-home assignment. Refrain from leaving any critical/sensitive content here.`,
  `This platform is NOT secured. All your (potentially) shady links are visible by all who visit this site.`,
  `The author(s) of this app is/are NOT responsible for any controversial/damaging content that ends up listed on this application.`,
]

const getUrls = () => fetch("/api/v1/urls").then((res) => {
  if (res.ok) {
    return res.json()
  }
  return res.text().then(errMsg => {
    throw new String(`${res.status} ${res.statusText}: ${errMsg}`)
  })
})

const addUrl = url => fetch("/api/v1/url", {
  method: "POST",
  headers: {
    "Content-Type": "text/plain",
  },
  body: url,
}).then((res) => {
  if (res.ok) {
    return res.text()
  }
  return res.text().then((errMsg) => {
    throw new String(`${res.status} ${res.statusText}: ${errMsg}`)
  })
})

const removeUrl = id => fetch(`/api/v1/url/${id}`, {
  method: "DELETE"
}).then(res => {
  if (res.ok) {
    return null
  }
  return res.text().then(errMsg => {
    throw new String(`${res.status} ${res.statusText}: ${errMsg}`)
  })
})

const Footer = ({ children }) => createPortal(children, document.getElementById("footer"))

const App = () => {
  const [urls, setUrls] = useState([])
  const [generatedUrl, setGeneratedUrl] = useState("")
  const [destUrl, setDestUrl] = useState("")
  const [appErr, setAppErr] = useState("")
  const [urlToDelete, setUrlToDelete] = useState(null)
  const [showDisclaimer, setShowDisclaimer] = useState(false)
  useEffect(() => {
    getUrls()
      .then(shortUrls => setUrls(shortUrls))
      .catch(errMsg => {
        console.log(errMsg)
        setAppErr("Unable to fetch short URL list.")
      })
  }, [])

  return (
    <>
      <ThemeProvider theme={appTheme}>
        <CssBaseline />
        <Grid
          container
          direction="column"
          justifyContent="center"
          alignItems="center"
          sx={{
            height: "100vh",
            margin: 0,
            padding: 0,
          }}
        >
          {/* Title */}
          <Grid item>
            <Typography variant="h1">ShortURLy</Typography>
          </Grid>
          <Grid item>
            <Button onClick={(() => () => setShowDisclaimer(true))()}>Disclaimer*</Button>
          </Grid>

          {/* Form for generating short URL */}
          <Grid item>
            <Grid
              container
              component="form"
              alignItems="center"
              direction="column"
              onSubmit={(() => (e) => {
                e.preventDefault()
                addUrl(destUrl).then(shortUrl => {
                  setUrls([{url: shortUrl, destination_url: destUrl}].concat(urls))
                  setGeneratedUrl(shortUrl)
                  setDestUrl("")
                }).catch(errMsg => {
                  console.log(errMsg)
                  setAppErr("Unable to generate short URL")
                })
              })()}
            >
              <Grid item>
                <TextField
                  required
                  type="url"
                  label="Target URL"
                  value={destUrl}
                  onChange={(() => (e) => setDestUrl(e.target.value))()}
                  helperText="URL to shorten"
                  placeholder="https://www.google.com"
                  InputProps={{
                    size: "128",
                  }}
                />
              </Grid>
              <Grid item component={Button} type="submit">Generate</Grid>
            </Grid>
          </Grid>

          {/* Display generated short URL */}
          {generatedUrl !== "" ? (
            <Grid item>
              <Typography display="inline" component="span">Your short URL is: </Typography>
              <Typography display="inline" component={Link} href={generatedUrl} target="_blank" rel="noopener">{generatedUrl}</Typography>
            </Grid>
          ) : null}

          {/* Short URL list */}
          <Grid item>
            <Grid container direction="column">
              <Grid item>
                <Typography variant="h3">Short URLs</Typography>
                <Typography>Use at your own risk.</Typography>
              </Grid>
              <Grid item>
                <TableContainer component={Paper}>
                  <Table>
                    <TableHead>
                      <TableRow>
                        <TableCell>Short URL</TableCell>
                        <TableCell>Destination URL</TableCell>
                        <TableCell>Action(s)</TableCell>
                      </TableRow>
                      
                    </TableHead>
                    <TableBody>
                      {urls.map(({url, destination_url}) => (
                        <TableRow key={url}>
                          <TableCell>
                            <Link href={url} target="_blank" rel="noopener">{url.substring(url.lastIndexOf("/")+1)}</Link>
                          </TableCell>
                          <TableCell>{destination_url}</TableCell>
                          <TableCell>
                            <IconButton
                              size="small"
                              color="error"
                              onClick={(() => () => setUrlToDelete({url, destination_url}))()}
                            >
                              <DeleteIcon fontSize="small" />
                            </IconButton>
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </TableContainer>
              </Grid>
            </Grid>
          </Grid>
        </Grid>

        {/* Show any error message */}
        <Snackbar
          open={appErr !== ""}
          onClose={(() => () => setAppErr(""))()}
          message={appErr}
          action={<IconButton
            size="small"
            aria-label="close"
            onClick={(() => () => setAppErr(""))()}
          >
            <CloseIcon fontSize="small" />
          </IconButton>}
        />

        {/* Confirmation prompt for deleting short URLs */}
        <Dialog
          open={urlToDelete !== null}
        >
          <DialogTitle>Remove short URL {urlToDelete ? urlToDelete.url.substring(urlToDelete.url.lastIndexOf("/")+1) : ""}?</DialogTitle>
          <DialogContent>
            <DialogContentText>This action is irreversible!!!</DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button color="success" onClick={(() => () => setUrlToDelete(null))()}>Maybe not</Button>
            <Button color="error" onClick={(() => () => {
              removeUrl(urlToDelete.url.substring(urlToDelete.url.lastIndexOf("/")+1))
              .then(() => setUrls(urls.filter(({url: sUrl}) => (sUrl !== urlToDelete.url))))
              .catch(errMsg => {
                console.log(errMsg)
                setAppErr("Failed to remove short URL")
              }).finally(() => setUrlToDelete(null))
            })()}>Yes</Button>
          </DialogActions>
        </Dialog>

        {/* Disclaimer dialog */}
        <Dialog
          open={showDisclaimer}
          scroll="paper"
        >
          <DialogTitle>Disclaimer (Please Read Carefully)</DialogTitle>
          <DialogContent dividers>
            <ul>
              {disclaimers.map((d, i) => (
                <li key={i}>{d}</li>
              ))}
            </ul>
          </DialogContent>
          <DialogActions>
            <Button onClick={(() => () => setShowDisclaimer(false))()}>Close</Button>
          </DialogActions>
        </Dialog>

        {/* Credits footer */}
        <Footer>
          <Grid container justifyContent="center" spacing={1}>
            <Grid item>
              <Typography variant="caption">Made by Qua Zi Xian</Typography>
            </Grid>
            <Grid item>
              <Grid container alignItems="center">
                <Grid item>
                  <LinkedInIcon fontSize="small" />
                </Grid>
                <Grid item>
                  <Typography variant="caption" display="inline" component={Link} href="https://www.linkedin.com/in/zixianq/" target="_blank" rel="noopener">
                    Zi Xian Qua
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Footer>
      </ThemeProvider>
    </>
  )
}

render((
  <App />
), document.getElementById("app"))