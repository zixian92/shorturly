"use strict"

const path = require("path")
const HtmlWebpackPlugin = require("html-webpack-plugin")

module.exports = {
  entry: path.join(__dirname, "app.jsx"),
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "[name]-[fullhash].js",
    publicPath: "/ui/",
    clean: true,
  },
  target: "web",
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, "index.html"),
      inject: "body",
    }),
  ],
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        options: {
          presets: ["@babel/preset-env", ["@babel/preset-react", { "runtime": "automatic" }]],
        }
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(png|jpg|jpeg|gif)$/,
        use: [{
          loader: "url-loader",
          options: {
            // No inlining for images larger than 10KB
            limit: 10 * 1024,
            name: "img/image-[hash].[ext]",
          },
        }],
      },
      {
        test: /\.svg$/,
        loader: "svg-url-loader",
        options: {
          limit: 10 * 1024,
        },
      },
    ]
  },
  watchOptions: {
    ignored: /node_modules/,
  },
}