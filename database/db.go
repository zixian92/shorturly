package database

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

// InitDB sets up the application's global database connection instance
// Only call this once throughout the application's lifetime,
// preferably at the start
// Returns the initialized database connection on success
// Returns non-nil error on failure
func InitDB(config Config) (*sql.DB, error) {
	var err error
	dsn := craftDSN(config)
	switch config.Type {
	case DatabaseTypeMySQL:
		db, err = sql.Open("mysql", dsn)
	default:
		return nil, fmt.Errorf("invalid database type %s", config.Type)
	}

	if err != nil {
		return nil, fmt.Errorf("error connecting to database: %v", err)
	}

	return db, nil
}

// GetDB returns the application-global database connection
func GetDB() *sql.DB {
	return db
}

// Shutdown terminates the application-global database connection
// Returns non-nil error on failure
func Shutdown() error {
	if db != nil {
		return db.Close()
	}
	return nil
}

func craftDSN(config Config) string {
	// Set up username and password part
	var userPass string
	if config.User == "" {
		config.User = "root"
	}
	if config.Password == "" {
		userPass = config.User
	} else {
		userPass = fmt.Sprintf("%s:%s", config.User, config.Password)
	}

	if config.Protocol == "" {
		config.Protocol = DatabaseProtocolTCP
	}

	var hostPort string
	if config.Host == "" {
		config.Host = "localhost"
	}
	if config.Port == 0 {
		hostPort = config.Host
	} else {
		hostPort = fmt.Sprintf("%s:%d", config.Host, config.Port)
	}

	return fmt.Sprintf("%s@%s(%s)/%s?parseTime=true", userPass, config.Protocol, hostPort, config.Database)
}
