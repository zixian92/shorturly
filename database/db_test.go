package database

import "testing"

func Test_craftDSN(t *testing.T) {
	type args struct {
		config Config
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
		{
			name: "fully specified",
			args: args{
				config: Config{
					Type:     DatabaseTypeMySQL,
					Protocol: DatabaseProtocolTCP,
					Host:     "mydb",
					Port:     3307,
					User:     "testuser",
					Password: "password",
					Database: "testdb",
				},
			},
			want: "testuser:password@tcp(mydb:3307)/testdb?parseTime=true",
		},
		{
			name: "unix socket",
			args: args{
				config: Config{
					Type:     DatabaseTypeMySQL,
					Protocol: DatabaseProtocolUnix,
					User:     "testuser",
					Database: "testdb",
				},
			},
			want: "testuser@unix(localhost)/testdb?parseTime=true",
		},
		{
			name: "all defaults",
			args: args{
				config: Config{},
			},
			want: "root@tcp(localhost)/?parseTime=true",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := craftDSN(tt.args.config); got != tt.want {
				t.Errorf("craftDSN() = %v, want %v", got, tt.want)
			}
		})
	}
}
