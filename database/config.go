package database

// DatabaseType determines the type of database to use
// Currently only mysql is supported
type DatabaseType string

// DatabaseTypeMySQL defines the value to use when using MySQL database
const DatabaseTypeMySQL DatabaseType = "mysql"

// DatabaseProtocol specifies the connection type used to connect to the database
type DatabaseProtocol string

const (
	// DatabaseProtocolTCP specifies database connection over TCP
	DatabaseProtocolTCP DatabaseProtocol = "tcp"

	// DatabaseProtocolUnix specieies database connection over Unix socket
	DatabaseProtocolUnix DatabaseProtocol = "unix"
)

// Config defines the configuration object used to set up the
// application's database connection
type Config struct {
	// The type of database to use
	Type DatabaseType

	// The database connection protocol
	Protocol DatabaseProtocol

	// Database host
	// Defaults to "localhost"
	Host string

	// Database port
	Port uint16

	// Database user
	// Defaults to "root"
	User string

	// Database user's password
	Password string

	// Database name to use
	Database string
}
