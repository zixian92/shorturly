variables:
  APP_IMAGE_NAME: registry.gitlab.com/zixian92/shorturly
  DEPLOY_IMAGE_NAME: registry.gitlab.com/zixian92/shorturly/deploy

stages:
- build
- test
- deploy
- release

# Build job for production-related tags/branches
build_pd:
  stage: build
  image: docker:20.10.11-alpine3.14
  services:
  - name: docker:20.10.11-dind-alpine3.14
    alias: docker
  tags:
  - linux
  - docker
  only:
  - /^\d+\.\d+\.\d+$/
  - /^\d+\.\d+\.\d+-staging$/
  - /^\d+\.\d+\.\d+-hotfix$/
  script:
  - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  - docker build --force-rm --pull --build-arg VERSION=$(echo $CI_COMMIT_REF_NAME | sed -E 's/([0-9]+\.[0-9]+\.[0-9]+).*/\1/') --file docker/Dockerfile --tag $APP_IMAGE_NAME:$CI_COMMIT_REF_NAME .
  - sh -c "[ -z $CI_COMMIT_TAG ] || docker tag $APP_IMAGE_NAME:$CI_COMMIT_TAG $APP_IMAGE_NAME:latest"
  - docker push $APP_IMAGE_NAME:$CI_COMMIT_REF_NAME
  - sh -c "[ -z $CI_COMMIT_TAG ] || docker push $APP_IMAGE_NAME:latest"

# Build image with all necessary client tools to manually deploy the application
# onto Kubernetes cluster
build_deployer:
  stage: build
  image: docker:20.10.11-alpine3.14
  services:
  - name: docker:20.10.11-dind-alpine3.14
    alias: docker
  tags:
  - linux
  - docker
  only:
  - /^\d+\.\d+\.\d+$/
  when: manual
  before_script:
  - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
  - docker build --force-rm --pull --file docker/Dockerfile-deploy --tag $DEPLOY_IMAGE_NAME docker
  - docker push $DEPLOY_IMAGE_NAME

# Job to check that client-side code builds without error
check_client_build:
  stage: build
  image: node:17-alpine3.14
  tags:
  - linux
  - docker
  only:
  - merge_requests
  before_script:
  - mkdir -p /src
  script:
  - cp -aRf $CI_PROJECT_DIR/ui/* /src/
  - cd /src
  - npm install
  - npm run build

# Job to check that server-side code compiles without error
check_server_build:
  stage: build
  image: golang:1.17-alpine3.15
  tags:
  - linux
  - docker
  only:
  - merge_requests
  variables:
    GO_PROJECT_PARENT: /go/src/gitlab.com/zixian92
  before_script:
  - mkdir -p ${GO_PROJECT_PARENT}
  script:
  - cp -aRf $CI_PROJECT_DIR ${GO_PROJECT_PARENT}/
  - cd ${GO_PROJECT_PARENT}/shorturly
  - go get -d -v ./...
  - go build -v .

# Job to run server-side unit tests
run_server_tests:
  stage: test
  image: golang:1.17-alpine3.15
  tags:
  - linux
  - docker
  only:
  - merge_requests
  - /^\d+\.\d+\.\d+$/
  - /^\d+\.\d+\.\d+-staging$/
  - /^\d+\.\d+\.\d+-hotfix$/
  variables:
    CGO_ENABLED: 0
    GO_PROJECT_PARENT: /go/src/gitlab.com/zixian92
  before_script:
  - mkdir -p ${GO_PROJECT_PARENT}
  script:
  - cp -aRf $CI_PROJECT_DIR ${GO_PROJECT_PARENT}/
  - cd ${GO_PROJECT_PARENT}/shorturly
  - go get -d -v ./...
  - go test -v -cover ./...

# Deploy to production Kubernetes cluster
deploy_pd:
  stage: deploy
  image: $DEPLOY_IMAGE_NAME
  environment: production
  tags:
  - linux
  - docker
  only:
  - /^\d+\.\d+\.\d+$/
  when: manual
  before_script:
  - kubectl config use-context zixian92/shorturly:shorturly-agent
  script:
  - envsubst < ${CI_PROJECT_DIR}/kubernetes/db.yaml | kubectl apply -f -
  - envsubst < ${CI_PROJECT_DIR}/kubernetes/web.yaml | kubectl apply -f -
  - sleep 10
  - kubectl get pods
  - kubectl get statefulsets
  - kubectl get deployments
  - kubectl get services
  - echo "Please check real-time status on the cluster itself before triggering release job."

# Create a release object on Gitlab
create_release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  tags:
  - linux
  - docker
  only:
  - /^\d+\.\d+\.\d+$/
  when: manual
  script:
  - echo "ShortURLy ${CI_COMMIT_TAG} is released!"
  release:
    tag_name: ${CI_COMMIT_TAG}
    name: "ShortURLy ${CI_COMMIT_TAG}"
    description: "Version ${CI_COMMIT_TAG} release for ShortURLy."