package store

import "errors"

const mysqlErrDuplicateKey uint16 = 1062

// ErrDuplicateKey defines the error returned by the data stores when an insertion
// results in duplicate identifier keys
var ErrDuplicateKey error = errors.New("another entry with the same identifier already exists")
