package store

import (
	"database/sql"
	"fmt"

	"github.com/go-sql-driver/mysql"
)

// ShortURL defines the mapping between a given URL and the generated short URL
type ShortURL struct {
	// The generated id hash for the long URL
	ID string

	// The actual long URL
	DestinationURL string
}

// ShortURLStore defines the data store for persisting short URL mappings
type ShortURLStore interface {
	// Add adds a new short URL mapping
	// Returns non-nil error on failure
	Add(shortURL ShortURL) error

	// Get retrieves the short URL mapping based on the given hashed ID
	// Returns non-nil error on failure
	// Returns nil if no such short URL mapping exists for the given ID
	Get(id string) (*ShortURL, error)

	// GetAll gets all short URL mappings
	// Returns non-nil error on failure
	GetAll() ([]ShortURL, error)

	// Remove removes the short URL mapping identified by the given hashed ID
	// Returns non-nil error on failure
	Remove(id string) error
}

type mysqlShortURLStore struct {
	db        *sql.DB
	tableName string
}

func (s *mysqlShortURLStore) Add(shortURL ShortURL) error {
	_, err := s.db.Exec(fmt.Sprintf(`INSERT INTO %s VALUES(?, ?)`, s.tableName), shortURL.ID, shortURL.DestinationURL)
	if mysqlErr, ok := err.(*mysql.MySQLError); ok {
		if mysqlErr.Number == mysqlErrDuplicateKey {
			return ErrDuplicateKey
		}
	} else if err != nil {
		return fmt.Errorf("failed to add new short URL mapping: %v", err)
	}
	return nil
}

func (s *mysqlShortURLStore) Get(id string) (*ShortURL, error) {
	row := s.db.QueryRow(fmt.Sprintf(`SELECT id, url FROM %s WHERE id = ?`, s.tableName), id)
	var shortURL ShortURL
	if err := row.Scan(&shortURL.ID, &shortURL.DestinationURL); err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, fmt.Errorf("failed to get short URL mapping for %s: %v", id, err)
	}
	return &shortURL, nil
}

func (s *mysqlShortURLStore) GetAll() ([]ShortURL, error) {
	rows, err := s.db.Query(fmt.Sprintf(`SELECT id, url FROM %s`, s.tableName))
	if err != nil {
		return nil, fmt.Errorf("failed to get short URL mapping list: %v", err)
	}

	shortURLs := []ShortURL{}
	hasErr := false
	var shortURL ShortURL
	for rows.Next() {
		if err := rows.Scan(&shortURL.ID, &shortURL.DestinationURL); err != nil {
			hasErr = true
		} else {
			shortURLs = append(shortURLs, shortURL)
		}
	}
	if hasErr {
		return shortURLs, fmt.Errorf("some short URL mappings have read errors")
	}
	return shortURLs, nil
}

func (s *mysqlShortURLStore) Remove(id string) error {
	_, err := s.db.Exec(fmt.Sprintf(`DELETE FROM %s WHERE id = ?`, s.tableName), id)
	if err != nil {
		return fmt.Errorf("failed to remove short URL mapping %s: %v", id, err)
	}
	return nil
}
