package store

import "database/sql"

var shortURLStore ShortURLStore

// InitStore sets up all data stores using the given database connection
// Call this function at the start of the application,
// before any code that uses this package is executed
func InitStores(db *sql.DB) {
	shortURLStore = &mysqlShortURLStore{
		db:        db,
		tableName: "shorturls",
	}
}

// GetShortURLStore gets the data store for handling short URL mappings
func GetShortURLStore() ShortURLStore {
	return shortURLStore
}
