package idgen

import (
	"math/rand"
	"time"
)

// IDGenerator defines a random ID string generator
type IDGenerator interface {
	// GenerateID generates an ID string of length len
	GenerateID(len int) string
}

// NewIDGenerator creates a new random ID string generator
// with which to generate ID strings restricted to only the characters
// in the specified charset
// charset should not contain any multibyte character
func NewIDGenerator(charset string) IDGenerator {
	return &idGenerator{
		charset:     charset,
		charsetSize: len(charset),
		rng:         rand.New(rand.NewSource(time.Now().Unix())),
	}
}

type idGenerator struct {
	charset     string
	charsetSize int
	rng         *rand.Rand
}

func (g *idGenerator) GenerateID(len int) string {
	idBytes := make([]byte, 0, len)

	for ; len > 0; len-- {
		idx := g.rng.Intn(g.charsetSize)
		idBytes = append(idBytes, g.charset[idx])
	}

	return string(idBytes)
}
