package idgen

import (
	"math/rand"
	"strings"
	"testing"
	"time"
)

func Test_idGenerator_GenerateID(t *testing.T) {
	const charset = "sdjkhugvbyr"
	generator := NewIDGenerator(charset)
	lenGenerator := rand.New(rand.NewSource(time.Now().Unix()))

	for i := 0; i < 10; i++ {
		idLen := lenGenerator.Intn(10)
		id := generator.GenerateID(idLen)
		realIDLen := len(id)
		if realIDLen != idLen {
			t.Errorf("expected ID string of length %d, but got one of length %d", idLen, realIDLen)
		}
		for _, c := range id {
			if !strings.ContainsRune(charset, c) {
				t.Errorf("generated ID string contains character(%s) not in the specified charset(%s)", string(c), charset)
			}
		}
	}
}
