package api

import "net/http"

type route struct {
	path    string
	methods []string
	handler http.Handler
}

var v1Routes = []route{
	{
		path:    "/url",
		methods: []string{"POST"},
		handler: http.HandlerFunc(addURL),
	},
	{
		path:    "/urls",
		methods: []string{"GET"},
		handler: http.HandlerFunc(getURLs),
	},
	{
		path:    "/url/{id}",
		methods: []string{"DELETE"},
		handler: http.HandlerFunc(removeURL),
	},
}

var routes = map[string][]route{
	"/v1": v1Routes,
}
