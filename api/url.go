package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/zixian92/shorturly/idgen"
	"gitlab.com/zixian92/shorturly/store"
)

const charPool = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

var urlIDGenerator = idgen.NewIDGenerator(charPool)

type shortURL struct {
	URL            string `json:"url"`
	DestinationURL string `json:"destination_url"`
}

func addURL(w http.ResponseWriter, r *http.Request) {
	urlBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logger.Printf("failed to read destination URL from request body: %v", err)
		http.Error(w, "unable to read URL from request body", http.StatusInternalServerError)
		return
	}

	var scheme string
	if r.TLS == nil {
		scheme = "http"
	} else {
		scheme = "https"
	}
	urlMap := store.ShortURL{
		DestinationURL: string(urlBytes),
	}
	for {
		urlMap.ID = urlIDGenerator.GenerateID(8)
		if err := store.GetShortURLStore().Add(urlMap); err == nil {
			w.Write([]byte(fmt.Sprintf("%s://%s/%s", scheme, r.Host, urlMap.ID)))
			return
		} else if err == store.ErrDuplicateKey {
			continue
		} else {
			logger.Printf("failed to save new short URL: %v", err)
			http.Error(w, "server error saving generated short URL", http.StatusInternalServerError)
			return
		}
	}
}

func getURLs(w http.ResponseWriter, r *http.Request) {
	urls, err := store.GetShortURLStore().GetAll()
	if err != nil {
		if urls == nil {
			logger.Printf("failed to get short URL list: %v", err)
			http.Error(w, "server error getting short URL list", http.StatusInternalServerError)
			return
		}
	}

	var scheme string
	if r.TLS == nil {
		scheme = "http"
	} else {
		scheme = "https"
	}
	resURLs := []shortURL{}
	for _, url := range urls {
		resURLs = append(resURLs, shortURL{
			URL:            fmt.Sprintf("%s://%s/%s", scheme, r.Host, url.ID),
			DestinationURL: url.DestinationURL,
		})
	}

	if err := json.NewEncoder(w).Encode(resURLs); err != nil {
		logger.Printf("failed to encode short URL list response into JSON: %v", err)
		http.Error(w, "server error encoding response", http.StatusInternalServerError)
	}
}

func removeURL(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	if err := store.GetShortURLStore().Remove(id); err != nil {
		logger.Printf("failed to remove short URL: %v", err)
		http.Error(w, "server error while removing URL", http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}
