package api

import (
	"log"

	"github.com/gorilla/mux"
)

var logger *log.Logger

// InitAPIRoutes takes the given router and sets up (sub)routes
// under it
func InitAPIRoutes(r *mux.Router, apiLogger *log.Logger) {
	logger = apiLogger
	for prefix, subRoutes := range routes {
		s := r.PathPrefix(prefix).Subrouter()
		for _, subRoute := range subRoutes {
			s.Handle(subRoute.path, subRoute.handler).Methods(subRoute.methods...)
		}
	}
}
