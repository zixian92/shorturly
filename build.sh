#!/bin/sh

docker run -it --rm -v ${PWD}:/go/src/gitlab.com/zixian92/shorturly \
-w /go/src/gitlab.com/zixian92/shorturly \
-e GOOS=linux \
-e GOARCH=amd64 \
golang:1.17-alpine3.15 \
sh -c "go get -d -v ./...; go build -v ."